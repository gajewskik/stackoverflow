package gajewski.krzysztof.stackoverflow.modules.details.details;

import java.util.ArrayList;

import gajewski.krzysztof.stackoverflow.model.answer.AnswerListDataSource;
import gajewski.krzysztof.stackoverflow.model.base.IDataSourceCallback;
import gajewski.krzysztof.stackoverflow.model.question.Question;
import gajewski.krzysztof.stackoverflow.model.question.QuestionDataSource;

/**
 * Created by Kaszmir on 29.11.2017.
 */

public class QuestionDetailsActivityPresenter implements IQuestionDetailsActivityPresenterCallback {

    private IQuestionDetailsActivityViewCallback viewCallback;
    private Question question;

    private QuestionDataSource questionDataSource;
    private AnswerListDataSource answerListDataSource;


    @Override
    public void setViewCallback(IQuestionDetailsActivityViewCallback callback) {
        this.viewCallback = callback;
    }

    @Override
    public void setQuestionData(Question question) {
        this.question = question;

        //create endpoints
        this.questionDataSource = new QuestionDataSource(this.viewCallback.getContext(), question.getQuestionId());
        this.answerListDataSource = new AnswerListDataSource(this.viewCallback.getContext(), question.getQuestionId());
    }

    @Override
    public ArrayList getItemList() {
        return this.answerListDataSource.getItemsList();
    }

    @Override
    public void viewLoaded() {
        if(this.viewCallback != null) {
            viewCallback.configAnswerList();
            fetchQuestion();
        }
    }


    /*
    PRIVATE METHODS
     */

    //fetch question by id from api
    //we need fetch again question because we have to avoid transaction too large error
    //which could be occur with body (sometimes large html) question passing
    private void fetchQuestion() {

        this.questionDataSource.fetchData(new IDataSourceCallback() {
            @Override
            public void fetchCompleted(boolean success, int errorCode, int itemsCount) {
                if(success) {
                    question = (Question) questionDataSource.getItem();
                    viewCallback.configQuestionView(question);
                    fetchAnswers();
                }else {
                    viewCallback.showErrorDialog();
                }
            }
        });

    }

    //fetch answers for question by id
    private void fetchAnswers() {
        this.answerListDataSource.fetch(new IDataSourceCallback() {
            @Override
            public void fetchCompleted(boolean success, int errorCode, int itemsCount) {
                if(success) {
                    viewCallback.updateAnswerList();
                }else{
                    viewCallback.showErrorDialog();
                }
            }
        });
    }
}
