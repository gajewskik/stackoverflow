package gajewski.krzysztof.stackoverflow.modules.list;

import android.content.Context;

import gajewski.krzysztof.stackoverflow.model.question.Question;

/**
 * Created by Kaszmir on 27.11.2017.
 * presenter -> view communication
 */

public interface IListActivityViewCallback {

    void configList();
    void refreshList();
    void showOptionsDialog(Question question);
    void showNoResultsInfo();
    void hideNoResultsInfo();
    Context getContext();

    void showErrorDialog();
}
