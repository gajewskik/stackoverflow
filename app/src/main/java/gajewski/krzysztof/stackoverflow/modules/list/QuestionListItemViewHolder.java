package gajewski.krzysztof.stackoverflow.modules.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import gajewski.krzysztof.stackoverflow.R;
import gajewski.krzysztof.stackoverflow.model.question.Question;
import gajewski.krzysztof.stackoverflow.view.CustomCircleImageView;

/**
 * Created by Kaszmir on 27.11.2017.
 */

public class QuestionListItemViewHolder extends RecyclerView.ViewHolder {


    /*
    Views
     */

    @BindView(R.id.question_list_item_view_holder_titleTextView)
    TextView titleTextView;

    @BindView(R.id.question_list_item_view_holder_userImageView)
    CustomCircleImageView userImageView;

    @BindView(R.id.question_list_item_view_holder_userNameTextView)
    TextView userNameTextView;


    //
    public QuestionListItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }


    //load data to view for each item of list
    public void configure(Context context, final Question question, final IQuestIconItemCallback callback) {
        if(question != null) {
            Picasso.with(context).load(question.getUserPhotoUrl()).into(this.userImageView);
            this.userNameTextView.setText(question.getUserName());
            this.titleTextView.setText(question.getQuestionTitle());

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.questionClicked(question);
                }
            });
        }
    }


    //
    interface IQuestIconItemCallback{
        void questionClicked(Question question);
    }


}
