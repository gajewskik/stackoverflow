package gajewski.krzysztof.stackoverflow.modules.details.details;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import gajewski.krzysztof.stackoverflow.R;
import gajewski.krzysztof.stackoverflow.model.answer.Answer;
import gajewski.krzysztof.stackoverflow.view.CustomCircleImageView;

/**
 * Created by Kaszmir on 29.11.2017.
 */

public class AnswerListItemViewHolder extends RecyclerView.ViewHolder {


    /*
    Views
     */

    @BindView(R.id.answer_list_item_view_holderAnswerAuthorImageView)
    CustomCircleImageView authorImageView;

    @BindView(R.id.answer_list_item_view_holderAnswerAuthorNameTextView)
    TextView authorNameTextView;

    @BindView(R.id.answer_list_item_view_holderAnswerTextView)
    TextView answerTextView;


    //
    public AnswerListItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }


    //set data to view from object
    public void configure(Context context, Answer answer) {
        if(answer != null) {

            Picasso.with(context).load(answer.getOwnerPhotoUrl()).into(this.authorImageView);
            this.authorNameTextView.setText(answer.getOwnerName());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                this.answerTextView.setText(Html.fromHtml(answer.getBody(), Html.FROM_HTML_MODE_COMPACT));
            }else {
                this.answerTextView.setText(Html.fromHtml(answer.getBody()));
            }
        }
    }
}
