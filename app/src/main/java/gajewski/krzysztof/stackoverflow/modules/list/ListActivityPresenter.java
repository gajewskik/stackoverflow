package gajewski.krzysztof.stackoverflow.modules.list;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

import gajewski.krzysztof.stackoverflow.model.base.Entity;
import gajewski.krzysztof.stackoverflow.model.base.IDataSourceCallback;
import gajewski.krzysztof.stackoverflow.model.question.Question;
import gajewski.krzysztof.stackoverflow.model.question.QuestionListDataSource;

/**
 * Created by Kaszmir on 27.11.2017.
 */

public class ListActivityPresenter implements IListActivityPresenterCallback {

    private String TAG = "ListActivityPresenter";

    private IListActivityViewCallback viewCallback;
    private QuestionListDataSource dataSource;

    /**
     IListActivityPresenterCallback
     */

    @Override
    public void setViewCallback(IListActivityViewCallback callback) {
        this.viewCallback = callback;
        this.dataSource = new QuestionListDataSource(callback.getContext());
    }

    @Override
    public void viewLoaded() {
        this.viewCallback.configList();
    }

    @Override
    public void queryChanged(String query) {
        this.viewCallback.hideNoResultsInfo();
        this.dataSource.setQuery(query);
        fetchData();
    }

    @Override
    public void refreshLayoutPulled() {
        this.viewCallback.hideNoResultsInfo();
        fetchData();
    }

    @Override
    public ArrayList getItemList() {
        return this.dataSource.getItemsList();
    }

    @Override
    public void questionClicked(Question question) {
        this.viewCallback.showOptionsDialog(question);
    }

    @Override
    public void saveState(Bundle outState) {
        saveData(outState);
    }

    @Override
    public void restoreState(Bundle savedState) {
        restoreData(savedState);
    }


    /**
     * PRIVATE METHODS
     */

    //fetch data from api for specific query
    private void fetchData(){
        this.dataSource.fetch(new IDataSourceCallback() {
            @Override
            public void fetchCompleted(boolean success, int errorCode, int itemsCount) {
                if(success) {
                    viewCallback.refreshList();

                    if(dataSource.getItemsCount() == 0) {
                        viewCallback.showNoResultsInfo();
                    }

                }else {
                    viewCallback.showErrorDialog();
                }
            }
        });
    }

    //save result list when view is destroyed
    private void saveData(Bundle outState) {
        outState.putParcelableArrayList("QUESTION_LIST", this.dataSource.getItemsList());
    }

    //restore result list after view recreate
    private void restoreData(Bundle savedState) {
        this.dataSource.setItemsList(savedState.<Entity>getParcelableArrayList("QUESTION_LIST"));
        this.viewCallback.refreshList();
    }


}
