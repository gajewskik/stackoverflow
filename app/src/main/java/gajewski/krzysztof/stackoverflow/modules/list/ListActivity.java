package gajewski.krzysztof.stackoverflow.modules.list;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import gajewski.krzysztof.stackoverflow.R;
import gajewski.krzysztof.stackoverflow.model.question.Question;
import gajewski.krzysztof.stackoverflow.modules.details.details.QuestionDetailsActivity;
import gajewski.krzysztof.stackoverflow.modules.details.webViewDetails.QuestionDetailsWebViewActivity;
import gajewski.krzysztof.stackoverflow.view.QuestionDetailsOptionsDialog;

public class ListActivity extends AppCompatActivity implements IListActivityViewCallback, SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener, QuestionListAdapter.IQuestionListAdapterItemCallback, QuestionDetailsOptionsDialog.IQuestionDetailsCallback {

    private String TAG = "ListActivity";

    @BindView(R.id.main_activity_searchView)
    SearchView searchView;

    @BindView(R.id.main_activity_recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.main_activity_refreshLayout)
    SwipeRefreshLayout refreshLayout;

    @BindView(R.id.list_activity_loadingView)
    ProgressBar loadingView;

    @BindView(R.id.main_activity_empty_listTextView)
    TextView noResultsTextView;

    private IListActivityPresenterCallback presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Bind view
        ButterKnife.bind(this, getWindow().getDecorView());

        init();

        //restore state of activity after destroy
        if(savedInstanceState != null) {
            this.presenter.restoreState(savedInstanceState);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.searchView.clearFocus();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //save state of activity
        this.presenter.saveState(outState);
    }


    /**
     * IListActivityViewCallback
     */

    @Override
    public Context getContext() {
        return ListActivity.this;
    }

    @Override
    public void showErrorDialog() {
        showAlertDialog();
    }

    @Override
    public void configList() {
        configureRecyclerView();
    }

    @Override
    public void refreshList() {
        this.refreshLayout.setRefreshing(false);
        this.loadingView.setVisibility(View.GONE);
        ((QuestionListAdapter) this.recyclerView.getAdapter()).updateData(presenter.getItemList());
    }

    @Override
    public void showOptionsDialog(Question question) {
        new QuestionDetailsOptionsDialog(ListActivity.this, question, this);
    }

    @Override
    public void showNoResultsInfo() {
        this.noResultsTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNoResultsInfo() {
        this.noResultsTextView.setVisibility(View.GONE);
    }


    /**
     * OptionsDialogCallback
     */

    @Override
    public void showInWebViewClicked(Question question) {
        showDetails(QuestionDetailsWebViewActivity.class, question);
    }

    @Override
    public void showInActivityClicked(Question question) {
        showDetails(QuestionDetailsActivity.class, question);
    }


    /**
     * ListAdapterCallback
     */

    @Override
    public void itemClicked(Question question) {
        this.presenter.questionClicked(question);
    }


    /**
     * OnRefreshListener
     */

    @Override
    public void onRefresh() {
        this.presenter.refreshLayoutPulled();
    }


    /**
     * SearchViewListener
     */

    @Override
    public boolean onQueryTextSubmit(String query) {
        this.loadingView.setVisibility(View.VISIBLE);
        this.presenter.queryChanged(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


    /**
     * PRIVATE METHODS
     */

    //create presenter, set listeners
    private void init() {

        //
        this.presenter = new ListActivityPresenter();
        this.presenter.setViewCallback(this);
        this.presenter.viewLoaded();

        //
        this.searchView.setOnQueryTextListener(this);
        this.searchView.onActionViewExpanded();
        this.searchView.setIconified(false);
        this.refreshLayout.setOnRefreshListener(this);
    }

    //base recyclerView configuration
    private void configureRecyclerView() {
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerView.setAdapter(new QuestionListAdapter(ListActivity.this, presenter.getItemList(), this));
        this.recyclerView.getAdapter().notifyDataSetChanged();
    }

    //navigate to specific activity
    private void showDetails(Class activityClass, Question question) {
        Intent i = new Intent(ListActivity.this, activityClass);

        if(activityClass == QuestionDetailsWebViewActivity.class) {
            i.putExtra("QUESTION_URL", question.getQuestionLink());
        }else {
            i.putExtra("QUESTION", question);
        }

        startActivity(i);
    }

    //
    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.load_data_error));
        builder.setMessage(getString(R.string.load_data_error_desc));
        builder.setPositiveButton(getString(R.string.load_data_error_confirm), null);
        builder.show();
    }


}
