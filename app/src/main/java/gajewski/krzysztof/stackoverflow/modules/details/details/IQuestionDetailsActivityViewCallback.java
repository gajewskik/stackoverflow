package gajewski.krzysztof.stackoverflow.modules.details.details;

import android.content.Context;

import gajewski.krzysztof.stackoverflow.model.question.Question;

/**
 * Created by Kaszmir on 29.11.2017.
 * Presenter -> View communication
 */

public interface IQuestionDetailsActivityViewCallback {

    Context getContext();
    void configQuestionView(Question question);
    void configAnswerList();
    void updateAnswerList();

    void showErrorDialog();
}
