package gajewski.krzysztof.stackoverflow.modules.list;

import android.os.Bundle;

import java.util.ArrayList;

import gajewski.krzysztof.stackoverflow.model.question.Question;

/**
 * Created by Kaszmir on 27.11.2017.
 * view -> presenter communication
 */

public interface IListActivityPresenterCallback {

    void setViewCallback(IListActivityViewCallback callback);
    void viewLoaded();
    void queryChanged(String query);
    void refreshLayoutPulled();

    ArrayList getItemList();

    void questionClicked(Question question);

    void saveState(Bundle outState);
    void restoreState(Bundle savedState);
}
