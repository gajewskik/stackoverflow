package gajewski.krzysztof.stackoverflow.modules.details.details;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import gajewski.krzysztof.stackoverflow.R;
import gajewski.krzysztof.stackoverflow.model.answer.Answer;

/**
 * Created by Kaszmir on 29.11.2017.
 * Adapter for Answer list
 */

public class AnswerListAdapter extends RecyclerView.Adapter {

    private Context context;
    private ArrayList itemList;


    //
    public AnswerListAdapter(Context context, ArrayList items) {
        this.context = context;
        this.itemList = items;
    }

    //
    public void updateData(ArrayList itemList) {
        this.itemList = itemList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //create itemViewHolder
        return new AnswerListItemViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.answer_list_item_view_holder, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Answer answer = (Answer) itemList.get(position);

        //viewHolder configuration
        ((AnswerListItemViewHolder) holder).configure(context, answer);
    }

    @Override
    public int getItemCount() {

        if(this.itemList != null) {
            return itemList.size();
        }else {
            return 0;
        }
    }
}
