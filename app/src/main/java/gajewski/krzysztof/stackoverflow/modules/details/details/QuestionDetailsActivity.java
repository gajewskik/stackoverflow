package gajewski.krzysztof.stackoverflow.modules.details.details;

import android.content.Context;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import gajewski.krzysztof.stackoverflow.R;
import gajewski.krzysztof.stackoverflow.model.question.Question;
import gajewski.krzysztof.stackoverflow.view.CustomCircleImageView;

public class QuestionDetailsActivity extends AppCompatActivity implements IQuestionDetailsActivityViewCallback {

    private String TAG = "QuestionDetailsActivity";

    /*
     * Views
     */

    @BindView(R.id.activity_question_details_ownerImageView)
    CustomCircleImageView ownerImageView;

    @BindView(R.id.activity_question_details_ownerNameTextView)
    TextView ownerNameTextView;

    @BindView(R.id.activity_question_details_questionTitleTextView)
    TextView questionTitleTextView;

    @BindView(R.id.activity_question_details_bodyTextView)
    TextView questionBodyTextView;

    @BindView(R.id.activity_question_details_answersRecyclerView)
    RecyclerView answersRecyclerView;

    @BindView(R.id.question_details_loadingView)
    ProgressBar loadingView;


    //
    private IQuestionDetailsActivityPresenterCallback presenter;


    /*
    Lifecycle area
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_details);
        configureToolbar();
        ButterKnife.bind(QuestionDetailsActivity.this, getWindow().getDecorView());

        //
        init();
    }



    //Handle back arrow click
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    /*
     * IQuestionDetailsActivityViewCallback
     */

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void configQuestionView(Question question) {
        setQuestionView(question);
    }

    @Override
    public void configAnswerList() {
        configRecyclerView();
    }

    @Override
    public void updateAnswerList() {
        this.loadingView.setVisibility(View.GONE);
        ((AnswerListAdapter)this.answersRecyclerView.getAdapter()).updateData(this.presenter.getItemList());
    }

    @Override
    public void showErrorDialog() {
        showAlertDialog();
    }


    /*
     * Private Methods
     */

    private void init() {

        //get object passed from list activity
        Question question = getIntent().getParcelableExtra("QUESTION");

        //create presenter set callback and data
        this.presenter = new QuestionDetailsActivityPresenter();
        this.presenter.setViewCallback(this);
        this.presenter.setQuestionData(question);
        this.presenter.viewLoaded();
    }


    //config question view
    private void setQuestionView(Question question) {

        Picasso.with(this).load(question.getUserPhotoUrl()).into(this.ownerImageView);
        this.ownerNameTextView.setText(question.getUserName());
        this.questionTitleTextView.setText(question.getQuestionTitle());


        if(question.getDesc() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                this.questionBodyTextView.setText(Html.fromHtml(question.getDesc(), Html.FROM_HTML_MODE_LEGACY));
            }else {
                this.questionBodyTextView.setText(Html.fromHtml(question.getDesc()));
            }
        }

    }


    //base config answers list
    private void configRecyclerView() {
        this.answersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.answersRecyclerView.setNestedScrollingEnabled(false);
        this.answersRecyclerView.setAdapter(new AnswerListAdapter(this, presenter.getItemList()));
        this.answersRecyclerView.getAdapter().notifyDataSetChanged();
    }


    //
    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.load_data_error));
        builder.setMessage(getString(R.string.load_data_error_desc));
        builder.setPositiveButton(getString(R.string.load_data_error_confirm), null);
        builder.show();
    }

    //
    private void showArrowBack() {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    //
    private void configureToolbar() {
        Toolbar toolbar = findViewById(R.id.activity_question_detailsToolbar);
        setSupportActionBar(toolbar);
        setTitle("");
        showArrowBack();
    }

}
