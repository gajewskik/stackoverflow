package gajewski.krzysztof.stackoverflow.modules.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import gajewski.krzysztof.stackoverflow.R;
import gajewski.krzysztof.stackoverflow.model.question.Question;

/**
 * Created by Kaszmir on 27.11.2017.
 */

public class QuestionListAdapter extends RecyclerView.Adapter {

    private String TAG = "QuestionListAdapter";

    private ArrayList itemList;
    private Context context;
    private IQuestionListAdapterItemCallback itemCallback;


    interface IQuestionListAdapterItemCallback{
        void itemClicked(Question question);
    }

    public QuestionListAdapter(Context context, ArrayList itemList, IQuestionListAdapterItemCallback callback) {
        this.itemList = itemList;
        this.context = context;
        this.itemCallback = callback;
    }

    public void updateData(ArrayList itemList) {
        this.itemList = itemList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //create itemViewHolder
        return new QuestionListItemViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.question_list_item_view_holder, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        //get entity at clicked position
        Question question = (Question) itemList.get(position);

        //config item with question data
        ((QuestionListItemViewHolder) holder).configure(context, question, new QuestionListItemViewHolder.IQuestIconItemCallback() {
            @Override
            public void questionClicked(Question question) {
                if(itemCallback != null) {
                    itemCallback.itemClicked(question);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if(this.itemList != null) {
            return this.itemList.size();
        }else {
            return 0;
        }
    }
}
