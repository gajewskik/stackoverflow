package gajewski.krzysztof.stackoverflow.modules.details.webViewDetails;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import gajewski.krzysztof.stackoverflow.R;

/**
 * The fastest way for show user question details with answers :)
 */
public class QuestionDetailsWebViewActivity extends AppCompatActivity  {


    @BindView(R.id.question_details_web_view_activity_webView)
    WebView webView;

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_details_web_view_activity);
        ButterKnife.bind(this, getWindow().getDecorView());
        configWebView();
    }

    private void configWebView() {
        this.url = getIntent().getStringExtra("QUESTION_URL");
        this.webView.loadUrl(this.url);
    }


}
