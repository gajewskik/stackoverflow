package gajewski.krzysztof.stackoverflow.modules.details.details;

import java.util.ArrayList;

import gajewski.krzysztof.stackoverflow.model.question.Question;

/**
 * Created by Kaszmir on 29.11.2017.
 * View -> Presenter communication
 */
public interface IQuestionDetailsActivityPresenterCallback {

    //
    void setViewCallback(IQuestionDetailsActivityViewCallback callback);

    //
    void setQuestionData(Question question);

    //
    ArrayList getItemList();

    //
    void viewLoaded();
}
