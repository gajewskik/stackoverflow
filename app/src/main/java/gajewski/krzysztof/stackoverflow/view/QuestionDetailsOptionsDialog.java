package gajewski.krzysztof.stackoverflow.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import gajewski.krzysztof.stackoverflow.R;
import gajewski.krzysztof.stackoverflow.model.question.Question;

/**
 * Created by Kaszmir on 29.11.2017.
 */

public class QuestionDetailsOptionsDialog extends Dialog {

    //
    public interface IQuestionDetailsCallback{
        void showInWebViewClicked(Question question);
        void showInActivityClicked(Question question);
    }

    public QuestionDetailsOptionsDialog(@NonNull Context context, final Question question, final IQuestionDetailsCallback callback) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.question_details_option_dialog);


        /*
        LISTENERS
         */

        findViewById(R.id.question_details_option_dialog_webViewTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(callback != null) {
                    callback.showInWebViewClicked(question);
                }
                dismiss();
            }
        });

        findViewById(R.id.question_details_option_dialog_activityTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(callback != null) {
                    callback.showInActivityClicked(question);
                }
                dismiss();
            }
        });

        findViewById(R.id.question_details_option_dialog_cancelTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        //
        show();
    }
}
