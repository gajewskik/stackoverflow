package gajewski.krzysztof.stackoverflow.model.base;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kaszmir on 27.11.2017.
 * For fetch single JsonObjects from API
 */

public class DataSource {

    private String TAG = "DataSource";

    private RequestQueue requestQueue;
    private Request request;
    private Entity item;
    private String url;

    public DataSource(Context context, String url) {
        this.requestQueue = Volley.newRequestQueue(context);
        this.setUrl(url);
    }

    // TO BE OVERRIDDEN

    public Entity makeEntity(JSONObject jsonObject) throws JSONException {
        return new Entity(jsonObject);
    }


    //

    public Entity getItem() {
        return this.item;
    }

    public void clearData() {
        this.item = null;
    }

    public void cancel() {
        if(this.request != null)
            this.request.cancel();
    }

    public void setUrl(String url) {
        this.url = ConnectionManager.BASE_URL + url;
    }


    public boolean fetchData(final IDataSourceCallback callback) {
        this.cancel();

        //make request
        this.request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    //encode response to utf-8
                    String resp = Html.fromHtml(response.toString()).toString();
                    JSONObject encodedResponse = new JSONObject(resp);
                    item = makeEntity(encodedResponse.getJSONArray("items").getJSONObject(0));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                callback.fetchCompleted(true, 0, 1);

                request = null;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                int statusCode = error.networkResponse != null ? error.networkResponse.statusCode : 404;
                callback.fetchCompleted(false, statusCode, 0);
                request = null;
            }
        });

        //increase timeout time
        this.request.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        this.requestQueue.add(this.request);

        return true;
    }

}

