package gajewski.krzysztof.stackoverflow.model.base;

import android.content.Context;
import android.text.Html;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kaszmir on 27.11.2017.
 * For fetch JsonArrays from API
 */

public class ListDataSource {

    private String TAG = "ListDataSource";

    protected RequestQueue requestQueue;
    protected Request request;

    // data to be shown in list etc.
    protected ArrayList<Entity> itemsList;
    protected String url;


    //Constructor
    public ListDataSource(String url, Context context) {
        this.requestQueue = Volley.newRequestQueue(context);
        this.itemsList = new ArrayList<>();
        this.setUrl(url);
    }

    //TO BE OVERRIDDEN (For make every entity from jsonObject))
    public Entity makeEntity(JSONObject jsonObject) throws JSONException {
        return new Entity(jsonObject);
    }

    public ArrayList<Entity> getItemsList() {
        return itemsList;
    }

    public Entity getItemAtPos(int pos) {
        return this.itemsList.get(pos);
    }

    public int getItemsCount() {

        if(this.itemsList == null) {
            this.itemsList = new ArrayList<>();
        }

        return this.itemsList.size();
    }

    public void setItemsList(ArrayList<Entity> entities) {
        this.itemsList = entities;
    }

    public void setUrl(String url) {
        this.url = ConnectionManager.BASE_URL + url;
        Log.d(TAG, "SET_URL = "+this.url);
    }

    public void fetch(IDataSourceCallback callback) {
        fetchData(this.url, callback);
    }

    public void cancelRequest() {
        if(this.request != null)
            this.request.cancel();
    }

    private boolean fetchData(final String url, final IDataSourceCallback callback) {

        this.cancelRequest();

        if(this.request != null) {
           Log.d(TAG, "REQUEST != null");
            return false;
        }

        int method = Request.Method.GET;

           Log.d(TAG, "CREATE REQUEST");
           Log.d(TAG, "URL = "+url);
        this.request = new JsonObjectRequest(method, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "REQUEST OK");
                Log.d(TAG, "" + response.toString());

                //clear item list when we start to fetch data
                itemsList.clear();


                JSONArray jsonArray = null;

                try {
                    String resp = Html.fromHtml(response.toString()).toString();
                    JSONObject encodedResponse = new JSONObject(resp);
                    jsonArray = encodedResponse.getJSONArray("items");

                    if(jsonArray != null) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            itemsList.add(makeEntity(jsonArray.getJSONObject(i)));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                callback.fetchCompleted(true, 0, response.length());

                request = null;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                int statusCode = error.networkResponse != null ? error.networkResponse.statusCode : 404;

                //
                callback.fetchCompleted(false, statusCode, 0);

                // fetch completed
                request = null;
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        //set more timeout
        this.request.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        this.requestQueue.add(this.request);

        return true;
    }

}
