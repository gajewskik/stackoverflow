package gajewski.krzysztof.stackoverflow.model.answer;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import gajewski.krzysztof.stackoverflow.model.base.Entity;

/**
 * Created by Kaszmir on 27.11.2017.
 */

public class Answer extends Entity implements Parcelable {

    private String ownerName;
    private String ownerPhotoUrl;
    private boolean isAccepted;
    private String body;

    //Parse JSONObject to JavaObject
    public Answer(JSONObject object) {
        super(object);

        try {this.ownerName = object.getJSONObject("owner").getString("display_name");} catch (JSONException e) {e.printStackTrace();}
        try {this.ownerPhotoUrl = object.getJSONObject("owner").getString("profile_image");} catch (JSONException e) {e.printStackTrace();}
        try {this.isAccepted = object.getBoolean("is_accepted");} catch (JSONException e) {e.printStackTrace();}
        try {this.body = object.getString("body");} catch (JSONException e) {e.printStackTrace();}

    }


    /*
    Parcelable implementation for save data in bundle
     */

    protected Answer(Parcel in) {
        super(in);
        ownerName = in.readString();
        ownerPhotoUrl = in.readString();
        isAccepted = in.readByte() != 0;
        body = in.readString();
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(ownerName);
        dest.writeString(ownerPhotoUrl);
        dest.writeByte((byte) (isAccepted ? 1 : 0));
        dest.writeString(body);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Answer> CREATOR = new Creator<Answer>() {
        @Override
        public Answer createFromParcel(Parcel in) {
            return new Answer(in);
        }

        @Override
        public Answer[] newArray(int size) {
            return new Answer[size];
        }
    };


    /*
    Getters
     */

    public String getOwnerName() {
        return ownerName;
    }

    public String getOwnerPhotoUrl() {
        return ownerPhotoUrl;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public String getBody() {
        return body;
    }

}
