package gajewski.krzysztof.stackoverflow.model.base;

/**
 * Created by Kaszmir on 28.11.2017.
 */

public interface IDataSourceCallback {
    void fetchCompleted(boolean success, int errorCode, int itemsCount);
}
