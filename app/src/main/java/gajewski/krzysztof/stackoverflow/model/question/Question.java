package gajewski.krzysztof.stackoverflow.model.question;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import gajewski.krzysztof.stackoverflow.model.base.Entity;

/**
 * Created by Kaszmir on 27.11.2017.
 * Single Question JSON Model
 */

public class Question extends Entity implements Parcelable {

    private int questionPoint;
    private int answerCount;
    private int questionId;
    private int score;
    private int lastActivityDate;
    private int creationDate;
    private String desc;
    private String questionTitle;
    private String userPhotoUrl;
    private String userName;
    private String questionLink;

    //Parse JSONObject to Java Object
    public Question(JSONObject object) {
        super(object);

        try {this.questionPoint = object.getInt("question_score");} catch (JSONException e) {e.printStackTrace();}
        try {this.answerCount = object.getInt("answer_count");} catch (JSONException e) {e.printStackTrace();}
        try {this.questionId = object.getInt("question_id");} catch (JSONException e) {e.printStackTrace();}
        try {this.score = object.getInt("score");} catch (JSONException e) {e.printStackTrace();}
        try {this.lastActivityDate = object.getInt("last_activity_date");} catch (JSONException e) {e.printStackTrace();}
        try {this.creationDate = object.getInt("creation_date");} catch (JSONException e) {e.printStackTrace();}
        try {this.desc = object.getString("body");} catch (JSONException e) {e.printStackTrace();}
        try {this.questionTitle = object.getString("title");} catch (JSONException e) {e.printStackTrace();}
        try {this.userPhotoUrl = object.getJSONObject("owner").getString("profile_image");} catch (JSONException e) {e.printStackTrace();}
        try {this.userName = object.getJSONObject("owner").getString("display_name");} catch (JSONException e) {e.printStackTrace();}
        try {this.questionLink = object.getString("link");} catch (JSONException e) {e.printStackTrace();}

    }


    /*
    Parcelable implementation for save and pass data in bundle
     */

    protected Question(Parcel in) {
        questionPoint = in.readInt();
        answerCount = in.readInt();
        questionId = in.readInt();
        score = in.readInt();
        lastActivityDate = in.readInt();
        creationDate = in.readInt();
        desc = in.readString();
        questionTitle = in.readString();
        userPhotoUrl = in.readString();
        userName = in.readString();
        questionLink = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(questionPoint);
        dest.writeInt(answerCount);
        dest.writeInt(questionId);
        dest.writeInt(score);
        dest.writeInt(lastActivityDate);
        dest.writeInt(creationDate);
        dest.writeString(desc);
        dest.writeString(questionTitle);
        dest.writeString(userPhotoUrl);
        dest.writeString(userName);
        dest.writeString(questionLink);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };


    /*
    Getters
     */

    public int getQuestionPoint() {
        return questionPoint;
    }

    public int getAnswerCount() {
        return answerCount;
    }

    public int getQuestionId() {
        return questionId;
    }

    public int getScore() {
        return score;
    }

    public int getLastActivityDate() {
        return lastActivityDate;
    }

    public int getCreationDate() {
        return creationDate;
    }

    public String getDesc() {
        return desc;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public String getQuestionLink() {
        return questionLink;
    }

    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public String getUserName() {
        return userName;
    }

}
