package gajewski.krzysztof.stackoverflow.model.answer;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import gajewski.krzysztof.stackoverflow.model.base.Entity;
import gajewski.krzysztof.stackoverflow.model.base.ListDataSource;

/**
 * Created by Kaszmir on 27.11.2017.
 * Endpoint for list of answers for question with specific id
 */

public class AnswerListDataSource extends ListDataSource {

    //
    public AnswerListDataSource(Context context, int questionId) {
        super("questions/"+questionId+"/answers?order=desc&sort=activity&site=stackoverflow&filter=withbody", context);
    }

    //
    @Override
    public Entity makeEntity(JSONObject jsonObject) throws JSONException {
        return new Answer(jsonObject);
    }
}
