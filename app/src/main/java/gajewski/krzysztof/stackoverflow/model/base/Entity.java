package gajewski.krzysztof.stackoverflow.model.base;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kaszmir on 27.11.2017.
 * Base entity class
 */

public class Entity implements Parcelable {

    protected int id;


    public Entity(JSONObject json) {

        // map values
        try {this.id = json.getInt("");} catch (JSONException e) {e.printStackTrace();}

    }

    public Entity() {
        this(0);
    }

    public Entity(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    /*
    PARCELABLE IMPLEMENTATION
     */

    protected Entity(Parcel in) {
        id = in.readInt();
    }

    public static final Creator<Entity> CREATOR = new Creator<Entity>() {
        @Override
        public Entity createFromParcel(Parcel in) {
            return new Entity(in);
        }

        @Override
        public Entity[] newArray(int size) {
            return new Entity[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
    }
}
