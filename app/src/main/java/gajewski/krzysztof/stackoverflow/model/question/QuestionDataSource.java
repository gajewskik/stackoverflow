package gajewski.krzysztof.stackoverflow.model.question;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import gajewski.krzysztof.stackoverflow.model.base.DataSource;
import gajewski.krzysztof.stackoverflow.model.base.Entity;

/**
 * Created by Kaszmir on 29.11.2017.
 * Endpoint for single question with specific id
 */

public class QuestionDataSource extends DataSource {

    public QuestionDataSource(Context context, int questionId) {
        super(context, "questions/"+questionId+"?order=desc&sort=activity&answers=1&site=stackoverflow&filter=withbody");
    }

    @Override
    public Entity makeEntity(JSONObject jsonObject) throws JSONException {
        return new Question(jsonObject);
    }
}
