package gajewski.krzysztof.stackoverflow.model.question;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;

import gajewski.krzysztof.stackoverflow.model.base.Entity;
import gajewski.krzysztof.stackoverflow.model.base.ListDataSource;

/**
 * Created by Kaszmir on 27.11.2017.
 * Endpoint for question list with desc which match with typed by user query.
 */

public class QuestionListDataSource extends ListDataSource {

    private String URL = "";

    public QuestionListDataSource(Context context) {
        super("", context);
    }

    //set query parameter for endpoint
    public void setQuery(String query) {
        try {
            URI uri = new URI(query.replace(" ", "%20"));
            this.URL = "search/advanced?order=desc&sort=activity&answers=1&q="+uri+"&site=stackoverflow";
            setUrl(this.URL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Entity makeEntity(JSONObject jsonObject) throws JSONException {
        return new Question(jsonObject);
    }
}
